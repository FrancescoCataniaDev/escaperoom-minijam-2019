﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFollowMousePosition : MonoBehaviour
{
    public RectTransform text;
    public Vector3 offSet;
    private Vector3 textPosition;
    
    
    void Update()
    {
        MoveText();
    }

    void MoveText()
    {
        textPosition = Input.mousePosition + offSet;
        text.position = textPosition;
    }
}
