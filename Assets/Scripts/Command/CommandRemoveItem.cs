﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandRemoveItem : Command {
	public const string Regex = @"remove\s(\w+)\s(\w+)";

	public CommandRemoveItem(GameDirector director) {
		this.director = director;
	}

	public override void Execute() {
		string actorId = parameters[0];
		string itemId = parameters[1];
		director.GetActor(actorId).RemoveItem(director.GetItem(itemId));
	}

	public override void Skip() { }

	public override void End() { }
}