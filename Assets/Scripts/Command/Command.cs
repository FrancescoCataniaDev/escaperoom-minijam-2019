﻿using System.Runtime.CompilerServices;

public abstract class Command {
	public bool running;
	public string[] parameters;
	public bool canBeSkipped = true;
	public bool instantaneous = false;
	protected GameDirector director;

	public abstract void Execute();

	public virtual void Skip() { }

	public virtual void End() { }
}