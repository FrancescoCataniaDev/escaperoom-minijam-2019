﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPlaySound : Command {
	public const string Regex = @"\|?play\s(\w+)";
	private AudioSource audioSource;
	private Coroutine checkCoroutine;
	private bool playing;

	public CommandPlaySound(GameDirector director) {
		this.director = director;
		director.Skip += Skip;
		audioSource = director.GetAudioSource();
	}

	private IEnumerator IsSoundPlaying() {
		while (audioSource.isPlaying) {
			yield return null;
		}
		End();
	}

	public override void Skip() {
		if (playing && canBeSkipped) {
			director.StopCoroutine(checkCoroutine);
			audioSource.Stop();
			End();
		}
	}

	public override void Execute() {
		playing = true;
		director.PlayAudio(parameters[0]);
		checkCoroutine = director.StartCoroutine(IsSoundPlaying());
	}

	public override void End() {
		director.Skip -= Skip;
		playing = false;
	}
}