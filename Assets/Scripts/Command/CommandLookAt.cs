﻿using System;

using UnityEngine;

public class CommandLookAt : Command {
	private Actor actor;
	private Vector3 target;
	public const string Regex = @"\|?look\s(\w+)\s((\w+)|(\(([-+]?[0-9]*[\.[0-9]+|[0-9]+]?),([-+]?[0-9]*[\.[0-9]+|[0-9]+]?),([-+]?[0-9]*[\.[0-9]+|[0-9]+]?)\)))(\s([-+]?[0-9]*[\.[0-9]+|[0-9]+]?))?";

	public CommandLookAt(GameDirector director) {
		this.director = director;
		director.Skip += Skip;
		canBeSkipped = true;
	}

	public override void Execute() {
		running = true;
		actor = director.GetActor(parameters[0]);
		target = new Vector3(Convert.ToSingle(parameters[1]), actor.transform.position.y, Convert.ToSingle(parameters[3]));
		actor.LookAt(target, parameters.Length > 4 ? Convert.ToSingle(parameters[4]) : 0.5f);
		actor.Stopped += End;
	}

	public override void Skip() {
		if (running && canBeSkipped) {
			actor.SkipLookAt();
			End();
		}
	}

	public override void End() {
		director.Skip -= Skip;
		actor.Stopped -= End;
		running = false;
	}
}