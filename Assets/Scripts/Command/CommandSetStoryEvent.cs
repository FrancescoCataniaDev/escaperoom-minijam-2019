﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandSetStoryEvent : Command {
	public const string Regex = @"set\s(\w+)\s([\w\W]+)";

	public CommandSetStoryEvent(GameDirector director) {
		this.director = director;
	}

	public override void Execute() {
		StoryEvent storyEvent = director.GetStoryEvent(parameters[0]);
		if (storyEvent != null) {
			storyEvent.value = parameters[1];
		}
		else {
			Debug.LogError($"Can't find Story Event [{storyEvent}]");
		}
	}
}