﻿using System.ComponentModel.Design;
using UnityEngine;

public class CommandTalk : Command {
	private static GameObject speechPrefab;
	private DialogueBubble dialogueBubble;
	private readonly UiManager uiManager;
	private Actor actor;

	public const string Regex = @"\|?talk\s(\w+)\s([\w\W]+)";

	public CommandTalk(GameDirector director) {
		this.director = director;
		uiManager = director.uiManager;
		director.Skip += Skip;
		if (speechPrefab == null) {
			speechPrefab = Resources.Load<GameObject>("UI/DialogueBubble");
		}
		canBeSkipped = true;
	}

	public override void Execute() {
		running = true;
		string actorId = parameters[0];
		string textToShow = parameters[1];
		actor = director.GetActor(actorId);
		GameObject speechBubble = Object.Instantiate(speechPrefab, uiManager.transform); // new GameObject("speech bubble");
		dialogueBubble = speechBubble.GetComponent<DialogueBubble>();
		dialogueBubble.speaker = actor;
		dialogueBubble.SetText(textToShow);
		dialogueBubble.AutoHide();
		dialogueBubble.Closed += End;
	}

	public override void Skip() {
		if (running && canBeSkipped) {
			dialogueBubble.Closed -= End;
			dialogueBubble.Skip();
			End();
		}
	}

	public override void End() {
		director.Skip -= Skip;
		running = false;
	}
}