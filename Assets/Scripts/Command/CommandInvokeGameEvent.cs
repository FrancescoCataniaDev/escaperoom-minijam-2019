﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvokeGameEvent : Command {
	public const string Regex = @"\|?event\s(\w+)";
	public CommandInvokeGameEvent(GameDirector director) {
		this.director = director;
	}

	public override void Execute() {
		director.InvokeEventInstantly(parameters[0]);
	}
}