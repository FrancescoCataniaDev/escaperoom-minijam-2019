﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandGetItem : Command {
	public const string Regex =  @"\|?get\s(\w+)\s(\w+)";
	
	//nei parametri il primo parametro l'id dell'item da aggiungere
	public CommandGetItem(GameDirector director) {
		this.director = director;
	}

	public override void Execute() {
		string actorId = parameters[0];
		string itemId = parameters[1];
		director.GetActor(actorId).AddItem(director.GetItem(itemId));
	}

	public override void Skip() { }

	public override void End() {}
}