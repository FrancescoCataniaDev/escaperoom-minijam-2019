﻿public class CommandCursorActivator : Command {
	private CursorManager cursorManager;
	public const string Regex = @"\|?cursor\s(game|other)";

	public CommandCursorActivator(GameDirector director) {
		this.director = director;
	}

	public override void Execute() {
		director.EnableCursorManager(parameters[0] == "game");
	}
}