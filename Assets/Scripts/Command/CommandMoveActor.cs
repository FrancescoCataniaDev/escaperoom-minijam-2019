﻿using System;
using UnityEngine;

public class CommandMoveActor : Command {
	private Actor actor;
	public const string Regex = @"\|?move\s(\w+)\s((\w+)|(\(([-+]?[0-9]*\.[0-9]+|[0-9]+),([-+]?[0-9]*\.[0-9]+|[0-9]+),([-+]?[0-9]*\.[0-9]+|[0-9]+)\)))";

	public CommandMoveActor(GameDirector director) {
		this.director = director;
		director.Skip += Skip;
		canBeSkipped = true;
		director.Interrupt += Interrupt;
	}

	public override void Execute() {
		running = true;
		string actorId = parameters[0];

		actor = director.GetActor(actorId);
		var targetPosition = new Vector3(Convert.ToSingle(parameters[1]), Convert.ToSingle(parameters[2]), Convert.ToSingle(parameters[3]));

		actor.matchRotation = null;
		if (parameters.Length > 4) {
			actor.matchRotation = new Quaternion(Convert.ToSingle(parameters[4]), Convert.ToSingle(parameters[5]), Convert.ToSingle(parameters[6]), Convert.ToSingle(parameters[7]));
		}

		actor.MoveTo(targetPosition);
		actor.Stopped += End;
	}

	private void Interrupt() {
		actor.StopMoveTo();
		End();
	}

	public override void Skip() {
		if (running && canBeSkipped) {
			actor.SkipMoveTo();
			End();
		}
	}

	public override void End() {
		director.Interrupt -= Interrupt;
		director.Skip -= Skip;
		running = false;
	}
}