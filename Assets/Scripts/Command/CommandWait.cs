﻿using System;
using System.Collections;
using UnityEngine;

public class CommandWait : Command {
	private Coroutine timerCoroutine;
	public const string Regex = @"\|?wait\s(([-+]?[0-9]*\.[0-9]+|[0-9]+))";

	public CommandWait(GameDirector director) {
		this.director = director;
		director.Skip += Skip;
		canBeSkipped = true;
	}

	public override void Execute() {
		timerCoroutine = director.StartCoroutine(Timer(Convert.ToSingle(parameters[0])));
	}

	private IEnumerator Timer(float duration) {
		running = true;
		yield return new WaitForSeconds(Convert.ToSingle(duration));
		End();
	}

	public override void Skip() {
		if (running && canBeSkipped) {
			if (timerCoroutine != null) {
				director.StopCoroutine(timerCoroutine);
			}
			End();
		}
	}

	public override void End() {
		director.Skip -= Skip;
		running = false;
	}
}