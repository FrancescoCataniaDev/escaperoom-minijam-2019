﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Player : Actor {
	public float idleVariationFrequency;
	private float idleParameter = 1;

	protected override void Awake() {
		base.Awake();
	}

	private void OnDisable() {
		StopAllCoroutines();
	}

	private void OnEnable() {
		StartCoroutine(ChangeIdleRandom());
	}

	private IEnumerator ChangeIdleRandom() {
		while (true) {
			float t = idleVariationFrequency;
			float startValue = idleParameter;
			float endValue = Random.Range(0, 1f);
			while (t > 0) {
				t -= Time.deltaTime;
				animator.SetFloat("Idle", Mathf.Lerp(startValue, endValue, 1 - t / idleVariationFrequency));
				yield return null;
			}
			idleParameter = endValue;
			yield return new WaitForSeconds(idleVariationFrequency);
		}
	}

}