﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Actor : MonoBehaviour, Interactive {
	[SerializeField] private string id = "id";
	public string ID {
		get => id;
	}
	public float speed;
	public float height = 2f;
	public Color speechColor = Color.white;
	public Inventory inventory;
	public Animator animator;
	[HideInInspector] public Quaternion? matchRotation;

	private NavMeshAgent agent;
	private NavMeshPath path;
	[SerializeField] private bool moving;
	private Vector3 destination;
	private Coroutine movingCoroutine;
	private Coroutine facingCoroutine;
	private bool canMove;
	public event Action MovingTo;
	public event Action Stopped;

	protected virtual void Awake() {
		agent = GetComponent<NavMeshAgent>();
		canMove = agent != null;
		if (canMove) {
			path = new NavMeshPath();
			agent.speed = speed;
			agent.updateRotation = true;
		}
		animator = GetComponentInChildren<Animator>();
		inventory = new Inventory();
	}

	public virtual void MoveTo(Vector3 newDestination) {
		if (canMove && !moving) {
			if (movingCoroutine != null) {
				StopCoroutine(movingCoroutine);
			}
			this.destination = newDestination;

			movingCoroutine = StartCoroutine(Moving());
		}
	}

	private IEnumerator Moving() {
		moving = true;
		MovingTo?.Invoke();
		animator.SetBool("IsMoving", true);
		agent.CalculatePath(destination, path);
		if (path.status != NavMeshPathStatus.PathInvalid) {
			Debug.Log($"Set path");
			//agent.SetPath(path);
			agent.SetDestination(destination);
		}
		else {
			Debug.Log($"Set destination");
			agent.SetDestination(destination);
		}
		yield return new WaitForSeconds(0.1f);
		while (moving) {
			if (agent.velocity.sqrMagnitude <= Mathf.Epsilon) {
				StopMoving();
			}
			yield return null;
		}
	}

	public virtual void SkipMoveTo() {
		if (moving) {
			StopCoroutine(movingCoroutine);
			if (agent.hasPath) {
				var path1 = agent.path;
				agent.Warp(path1.corners[path1.corners.Length - 1]);
			}
			if (matchRotation != null) {
				if (facingCoroutine != null) {
					StopCoroutine(facingCoroutine);
				}
				transform.rotation = (Quaternion)matchRotation;
			}
			StopMoving();
		}
	}

	public virtual void StopMoveTo() {
		if (moving) {
			StopCoroutine(movingCoroutine);
			matchRotation = null;
			StopMoving();
		}
	}

	private void StopMoving() {
		animator.SetBool("IsMoving", false);
		moving = false;
		if (matchRotation != null) {
			facingCoroutine = StartCoroutine(AlignRotation());
		}
		else {
			Stopped?.Invoke();
		}
	}

	public virtual void LookAt(Vector3 target, float duration = 0.5f) {
		matchRotation = Quaternion.LookRotation(target - transform.position);
		facingCoroutine = StartCoroutine(AlignRotation(duration));
	}

	public virtual void SkipLookAt() {
		if (facingCoroutine != null) {
			StopCoroutine(facingCoroutine);
			if (matchRotation != null) transform.rotation = (Quaternion)matchRotation;
			Stopped?.Invoke();
		}
	}

	public virtual void IsTalking(bool isTalking) {
		animator.SetBool("isTalking", isTalking);
	}

	private IEnumerator AlignRotation(float duration = .5f) {
		float timer = duration;
		Quaternion startingRotation = transform.rotation;
		//Debug.Log($"{name} rotates to {matchRotation}");
		while (timer > 0) {
			float t = (1f - timer) / duration;
			//Debug.Log($"{1-t/duration} {t} {duration}");
			if (matchRotation != null) {
				transform.rotation = Quaternion.Slerp(startingRotation, (Quaternion)matchRotation, t);
			}
			timer -= Time.deltaTime;
			yield return null;
		}
		if (matchRotation != null) {
			transform.rotation = (Quaternion)matchRotation;
		}
		Stopped?.Invoke();
	}

	public virtual void OnLeftClick() {
		Debug.Log($"Someone left clicked on {name}");
	}

	public virtual void OnRightClick(Item tool = null) {
		Debug.Log($"Someone right clicked on {name} with tool [{tool}]");
	}

	public virtual void AddItem(Item item) {
		inventory.AddItem(item);
	}

	public virtual void RemoveItem(Item item) {
		inventory.RemoveItem(item);
	}

	public bool HasItem(Item item) {
		return inventory.HasItem(item);
	}
}