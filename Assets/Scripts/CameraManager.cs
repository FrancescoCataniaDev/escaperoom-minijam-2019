﻿using Sirenix.OdinInspector;
using UnityEngine;

public class CameraManager : MonoBehaviour {
	private CameraPan cameraPan;
	private FollowPlayer followPlayer;
	private Vector3 defaultPosition;
	private Quaternion defaultRotation;

	private void Awake() {
		defaultPosition = transform.position;
		defaultRotation = transform.rotation;
		cameraPan = GetComponent<CameraPan>();
		followPlayer = GetComponent<FollowPlayer>();
	}

	[Button("Reset Position")]
	public void Reset() {
		transform.position = defaultPosition;
		transform.rotation = defaultRotation;
		cameraPan.Init();
		followPlayer.Reset();
	}

	public void Position(Transform newTransform) {
		transform.position = newTransform.position;
		transform.rotation = newTransform.rotation;
		cameraPan.Init();
		followPlayer.Reset();
	}
}