﻿using TMPro;
using UnityEngine;

public class HotspotBubble : MonoBehaviour {
	public string textToShow;
	private TMP_Text textArea;
	private RectTransform rectTransform;
	private CursorManager cursorManager;
	private new Camera camera;

	private void Awake() {
		rectTransform = GetComponent<RectTransform>();

		textArea = GetComponent<TMP_Text>();
		textArea.enabled = false;

		cursorManager = FindObjectOfType<CursorManager>();
		cursorManager.HoverTextUpdated += UpdateHoverText;
		cursorManager.HoverTextShown += Show;
		cursorManager.HoverTextHidden += Hide;
		camera = Camera.main;
	}

	private void OnDrawGizmos() {
		if (textArea) {
			Gizmos.color = Color.green;
			float w = textArea.renderedWidth;
			float h = textArea.renderedHeight;
			Gizmos.DrawWireCube(rectTransform.position + new Vector3(w / 2, h / 2, 0), new Vector3(w, h, 0));
		}
	}

	private void UpdateHoverText(string text) {
		textArea.text = text;
	}

	private void Show() {
		textArea.enabled = true;
	}

	private void Hide() {
		textArea.enabled = false;
	}

	private void Update() {
		rectTransform.position = CalculatePosition();
	}

	private Vector3 CalculatePosition() {
		float w = textArea.renderedWidth;
		float h = textArea.renderedHeight;
		Vector3 displayPosition = cursorManager.transform.position;
		if (displayPosition.x + w > camera.ViewportToScreenPoint(Vector3.right + Vector3.up).x) {
			displayPosition.x -= displayPosition.x + w - camera.ViewportToScreenPoint(Vector3.right + Vector3.up).x; //
		}
		else if (displayPosition.x < 10) {
			displayPosition.x = 10;
		}
		if (displayPosition.y + h > camera.ViewportToScreenPoint(Vector3.right + Vector3.up).y) {
			displayPosition.y -= displayPosition.y + h - camera.ViewportToScreenPoint(Vector3.right + Vector3.up).y; //
		}
		Canvas.ForceUpdateCanvases();
		return displayPosition;
	}
}