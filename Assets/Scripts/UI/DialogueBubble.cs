﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class DialogueBubble : MonoBehaviour {
	public Actor speaker;

	private TMP_Text textArea;
	private RectTransform rectTransform;
	private new Camera camera;

	public event Action Closed;

	private void Awake() {
		textArea = GetComponent<TMP_Text>();
		textArea.text = "";
		rectTransform = GetComponent<RectTransform>();
		camera = Camera.main;
	}

	private void OnEnable() {
		textArea.color = Color.clear;
	}

	private void Update() {
		rectTransform.position = CalculatePosition();
	}

#if UNITY_EDITOR
	private void OnDrawGizmos() {
		if (textArea) {
			Gizmos.color = Color.green;
			float w = textArea.renderedWidth;
			float h = textArea.renderedHeight;
			Gizmos.DrawWireCube(Camera.main.WorldToScreenPoint(speaker.transform.position + Vector3.up * speaker.height) + new Vector3(0, h / 2, 0), new Vector3(w, h, 0));
		}
	}
#endif

	private Vector3 CalculatePosition() {
		float w = textArea.renderedWidth;
		float h = textArea.renderedHeight;
		Vector3 displayPosition = (Vector2)Camera.main.WorldToScreenPoint(speaker.transform.position + Vector3.up * speaker.height) + new Vector2(-w / 2, h / 2);
		if (displayPosition.x + w > camera.ViewportToScreenPoint(Vector3.right + Vector3.up).x) {
			displayPosition.x -= displayPosition.x + w - camera.ViewportToScreenPoint(Vector3.right + Vector3.up).x; //
		}
		else if (displayPosition.x < 10) {
			displayPosition.x = 10;
		}
		if (displayPosition.y + h > camera.ViewportToScreenPoint(Vector3.right + Vector3.up).y) {
			displayPosition.y -= displayPosition.y + h - camera.ViewportToScreenPoint(Vector3.right + Vector3.up).y; //
		}
		Canvas.ForceUpdateCanvases();
		return displayPosition;
	}

	public void SetText(string newText) {
		rectTransform.position = CalculatePosition();
		textArea.text = newText;
		textArea.color = speaker.speechColor;
		speaker.IsTalking(true);
	}

	public void AutoHide() {
		StartCoroutine(Hiding(textArea.text.Length * 0.1f));
	}

	public void Skip() {
		StopAllCoroutines();
		Destroy(gameObject);
	}

	private IEnumerator Hiding(float delay) {
		yield return new WaitForSeconds(delay);
		Destroy(gameObject);
	}

	private void OnDestroy() {
		speaker.IsTalking(false);
		Closed?.Invoke();
	}
}