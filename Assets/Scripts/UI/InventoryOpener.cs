﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryOpener : MonoBehaviour, IPointerClickHandler {
	public event Action Clicked;

	public void OnPointerClick(PointerEventData eventData) {
		Clicked?.Invoke();
	}
}