﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keypad : MonoBehaviour
{
    public Text result;
    public string password;
    public List<Button> buttons;
    public SAudio keypad;
    private GameDirector gameDirector;
    private Animator animator;
    private AudioSource audioSource;
    
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        gameDirector = FindObjectOfType<GameDirector>();
    }

    public void AddNumber(string number)
    {
        keypad.Play(audioSource, 0);
        result.text += number;
        if (result.text.Length >= 3)
        {
            if (result.text == password)
            {
                Debug.Log("Hai indovinato");
                keypad.Play(audioSource, 2);
                animator.SetTrigger("CodeRight");
            }
            else
            {
                Debug.Log("Hai sbagliato password");
                keypad.Play(audioSource, 1);
                animator.SetTrigger("CodeWrong");
            }
        }
    }

    public void DeactivateKeypad()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            buttons[i].enabled = false;
        }
    }
    
    public void ActivateKeypad()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            buttons[i].enabled = true;
        }
        result.text = "";
    }

    public void CloseKeypad()
    {
        gameDirector.InterpretCommands("cursor game");
        gameDirector.ExecuteQueue();
        GetComponent<Canvas>().enabled = false;
    }

    public void CorrectPassword()
    {
        gameDirector.InterpretCommands("set DoorUnlocked true");
        gameDirector.ExecuteQueue();
        CloseKeypad();
    }

    public void PlayeSound(AudioClip audio)
    {
        audioSource.PlayOneShot(audio);
    }
}
