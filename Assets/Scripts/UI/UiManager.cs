﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UiManager : MonoBehaviour {
	private InventoryUi inventoryUi;
	private GameDirector director;
	private Player player;
	private Inventory inventory;
	private CursorManager cursorManager;
	private HotspotBubble hotspotBubble;
	private Canvas canvas;

	private void Awake() {
		director = FindObjectOfType<GameDirector>();
		canvas = GetComponent<Canvas>();
		inventoryUi = transform.GetComponentInChildren<InventoryUi>();
		cursorManager = transform.GetComponentInChildren<CursorManager>();
		hotspotBubble = transform.GetComponentInChildren<HotspotBubble>();
		//hotspotBubble = Instantiate(Resources.Load<HotspotBubble>("UI/HotspotBubble"), transform);
	}

	private void Start() {
		player = director.GetPlayer();
		inventory = player.inventory;
		inventory.Updated += inventoryUi.UpdateUI;
	}
}