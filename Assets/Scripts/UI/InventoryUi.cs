﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryUi : MonoBehaviour {
	public GameObject itemsContainer;
	public GameObject itemUiPrefab;

	public InventoryOpener inventoryOpener;
	public float openingTime = 0.5f;
	public bool open;
	private bool active;
	private float height;
	private RectTransform rectTransform;

	private readonly List<GameObject> uiItems = new List<GameObject>();

	private void Awake() {
		inventoryOpener.Clicked += OnOpenerClicked;
		rectTransform = GetComponent<RectTransform>();
		height = rectTransform.rect.height;
		rectTransform.anchoredPosition3D = new Vector3(rectTransform.anchoredPosition3D.x, open ? 0 : -height, rectTransform.anchoredPosition3D.z);
	}

	private void Start() {
		active = true;
	}

	public void UpdateUI(Dictionary<string, Item> items) {
		foreach (GameObject uiItem in uiItems) {
			Destroy(uiItem);
		}
		uiItems.Clear();

		for (int i = 0; i < items.Count; i++) {
			var item = items.ElementAt(i);
			var value = item.Value;
			var uiElement = Instantiate(itemUiPrefab, itemsContainer.transform);
			ItemManager itemManager = uiElement.GetComponent<ItemManager>();
			itemManager.item = value;
			itemManager.SetImage();
			uiItems.Add(uiElement);
		}
	}

	private void Update() {
		if (active) {
			if (Input.mouseScrollDelta.y > 0) {
				if (!open) OnOpenerClicked();
			}
			if (Input.mouseScrollDelta.y < 0) {
				if (open) OnOpenerClicked();
			}
		}
	}

	private void OnOpenerClicked() {
		StartCoroutine(ShowHide());
	}

	public void Disable() {
		active = false;
		if (open) OnOpenerClicked();
	}

	private IEnumerator ShowHide() {
		open = !open;
		Vector3 anchoredPosition = rectTransform.anchoredPosition3D;
		float startingY = anchoredPosition.y;
		float endingY = open ? 0 : -height;
		var startingPoint = new Vector3(anchoredPosition.x, startingY, anchoredPosition.z);
		var endingPoint = new Vector3(anchoredPosition.x, endingY, anchoredPosition.z);
		float t = openingTime;
		while (t > 0) {
			float d = 1 - t / openingTime;
			rectTransform.anchoredPosition3D = Vector3.Lerp(startingPoint, endingPoint, d * d * (3f - 2f * d));
			t -= Time.unscaledDeltaTime;
			yield return null;
		}

		yield return null;
	}
}