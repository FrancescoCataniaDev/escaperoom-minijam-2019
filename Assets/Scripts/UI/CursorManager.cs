﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public enum InteractionType {
	hand,
	eye,
	tool,
	cancel,
	wait
}

public class CursorManager : MonoBehaviour {
	[Serializable]
	public struct InteractionIcon {
		public Sprite sprite;
		public InteractionType interactionType;
	}

	private bool active = true;
	public InteractionType leftInteractionType;
	public InteractionType rightInteractionType;
	public InteractionIcon[] interactionIcons;

	public Image leftInteractionImage;
	public Image rightInteractionImage;

	public string hoverText;
	[HideInInspector] public bool eventsActive;

	//private bool onInteractive;
	private bool isInCinematic;
	private Dictionary<InteractionType, Sprite> interactionIconsDictionary = new Dictionary<InteractionType, Sprite>();

	private GameDirector gameDirector;
	private CanvasGroup canvasGroup;
	[SerializeField] private string targetId;
	[SerializeField] private string targetDescription;
	[SerializeField] private Item tool;
	private Hotspot[] hotspots;

	public event Action<Vector3, string, InteractionType, Item> RoomInteraction;
	public event Action<Vector3, string, InteractionType, Item> UiInteraction;
	public event Action HoverTextShown;
	public event Action HoverTextHidden;
	public event Action<string> HoverTextUpdated;

	private void Awake() {
		eventsActive = true;
		gameDirector = FindObjectOfType<GameDirector>();

		canvasGroup = GetComponent<CanvasGroup>();

		foreach (var interactionIcon in interactionIcons) {
			interactionIconsDictionary.Add(interactionIcon.interactionType, interactionIcon.sprite);
		}

		leftInteractionImage.sprite = interactionIconsDictionary[leftInteractionType];
		rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];

		isInCinematic = false;
		gameDirector.EnterCinematic += OnEnterCinematic;
		gameDirector.ExitCinematic += OnExitCinematic;
	}

	private void Start() {
		Cursor.visible = false;
		hotspots = gameDirector.GetHotspots();
		foreach (var hotspot in hotspots) {
			hotspot.MouseExit += MouseExitHotspot;
			hotspot.MouseOver += MouseOverHotspot;
		}
	}

	private void OnEnable() {
		
	}

	private void Update() {
		transform.position = Input.mousePosition;
		if (active) {
			if (Input.GetMouseButtonDown(0)) {
				if (EventSystem.current.IsPointerOverGameObject()) {
					UiInteraction?.Invoke(Input.mousePosition, targetId, leftInteractionType, tool);
				}
				else {
					RoomInteraction?.Invoke(Input.mousePosition, targetId, leftInteractionType, tool);
				}
			}
			if (Input.GetMouseButtonDown(1)) {
				if (rightInteractionType == InteractionType.cancel) {
					UnequipTool();
				}
				else {
					//Precedenza alla UI
					if (EventSystem.current.IsPointerOverGameObject()) {
						UiInteraction?.Invoke(Input.mousePosition, targetId, rightInteractionType, tool);
					}
					else {
						RoomInteraction?.Invoke(Input.mousePosition, targetId, rightInteractionType, tool);
					}
				}
			}
		}
		//canvasGroup.alpha = active ? 1 : 0.3f;
		if (isInCinematic) {
			HoverTextHidden?.Invoke();
		}
	}

	public void ShowActions() {
		if (active) {
			leftInteractionImage.enabled = rightInteractionImage.enabled = true;
		}
	}

	public void HideActions() {
		targetId = null;
		targetDescription = null;
		rightInteractionImage.enabled = false;
		leftInteractionImage.enabled = tool != null;
	}

	private void MouseOverHotspot(string targetId, string targetDescription) {
		SetTarget(targetId,targetDescription);
		ShowText();
		ShowActions();
	}

	private void MouseExitHotspot(string targetId) {
		UnsetTarget(targetId);
		HideText();
		HideActions();
	}

	public void SetTarget(string targetId, string targetDescription) {
		this.targetId = targetId;
		this.targetDescription = targetDescription;
		ComposeHover();
	}

	public void UnsetTarget(string targetId) {
		if (this.targetId == targetId) {
			this.targetId = null;
			targetDescription = null;
		}
		ComposeHover();
	}

	private void OnEnterCinematic() {
		leftInteractionType = InteractionType.wait;
		leftInteractionImage.sprite = interactionIconsDictionary[leftInteractionType];
		rightInteractionType = InteractionType.wait;
		rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];
		isInCinematic = true;
	}

	private void OnExitCinematic() {
		isInCinematic = false;
		if (tool != null && gameDirector.GetPlayer().HasItem(tool)) {
			leftInteractionType = InteractionType.tool;
			leftInteractionImage.sprite = tool.inventorySprite;
			rightInteractionType = InteractionType.cancel;
			rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];
		}
		else {
			leftInteractionType = InteractionType.hand;
			leftInteractionImage.sprite = interactionIconsDictionary[leftInteractionType];
			rightInteractionType = InteractionType.eye;
			rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];
		}
		ComposeHover();
	}

	public void ShowText() {
		if (!isInCinematic && active) {
			HoverTextShown?.Invoke();
		}
	}

	public void HideText() {
		if (tool == null) {
			HoverTextHidden?.Invoke();
		}
		else {
			ComposeHover();
		}
	}

	public void EquipTool(Item tool) {
		if (active) {
			this.tool = tool;
			leftInteractionImage.sprite = tool.inventorySprite;
			leftInteractionType = InteractionType.tool;
			rightInteractionType = InteractionType.cancel;
			rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];
			ComposeHover();
		}
	}

	public void UnequipTool() {
		leftInteractionType = InteractionType.hand;
		leftInteractionImage.sprite = interactionIconsDictionary[leftInteractionType];
		rightInteractionType = InteractionType.eye;
		rightInteractionImage.sprite = interactionIconsDictionary[rightInteractionType];
		tool = null;
		ComposeHover();
	}

	public void SetActive(bool newActive) {
		active = newActive;
		if (newActive == false) {
			HideActions();
			HideText();
		}
	}

	private void ComposeHover() {
		if (tool == null) {
			hoverText = $"{targetDescription}";
		}
		else {
			if (tool.ID != targetId) {
				hoverText = $"Use {tool.description} with {targetDescription}";
			}
			else {
				hoverText = $"Use {tool.description} with ";
			}
		}
		HoverTextUpdated?.Invoke(hoverText);
	}
}