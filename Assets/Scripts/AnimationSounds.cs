﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSounds : MonoBehaviour {
    public SAudio stepSound;
    private AudioSource audioSource;

    private void Awake() {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
    }

    public void Step() {
        stepSound.Play(audioSource);
    }
}
