﻿using UnityEngine;

public class CameraPositioner : MonoBehaviour {
	public Transform cameraTransform;
	private CameraManager cameraManager;

	private void Awake() {
		cameraManager = FindObjectOfType<CameraManager>();
	}

	private void OnDrawGizmos() {
		Gizmos.DrawLine(cameraTransform.position, cameraTransform.position + cameraTransform.forward);
	}

	private void OnTriggerEnter(Collider other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			cameraManager.Position(cameraTransform);
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			cameraManager.Reset();
		}
	}
}