﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Interactive {
	void OnLeftClick();
	void OnRightClick(Item tool = null);
}