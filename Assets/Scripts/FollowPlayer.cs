﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
	public SAudio movingSound;
	public bool lockX;
	public bool lockY;
	public bool lockZ;
	private Player player;
	private AudioSource audioSource;
	private Quaternion previousRotation;
	private float defaultX;
	private float defaultY;
	private float defaultZ;

	private void Awake() {
		player = FindObjectOfType<Player>();
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.loop = true;
		Reset();
	}

	private void Update() {
		LookAtObject(player.transform.position + Vector3.up);
		if (audioSource.isPlaying) {
			if (previousRotation == transform.rotation) {
				audioSource.Stop();
			}
		}
		else {
			if (previousRotation != transform.rotation && !audioSource.isPlaying) {
				movingSound.Play(audioSource);
			}
		}
		previousRotation = transform.rotation;
	}

	public void Reset() {
		previousRotation = transform.rotation;
		defaultX = (transform.position + transform.forward).x;
		defaultY = (transform.position + transform.forward).y;
		defaultZ = (transform.position + transform.forward).z;
	}

	private void LookAtObject(Vector3 objectToLookAt) {
		transform.LookAt(new Vector3(lockX ? defaultX : objectToLookAt.x, lockY ? defaultY : objectToLookAt.y, lockZ ? defaultZ : objectToLookAt.z));
	}
}