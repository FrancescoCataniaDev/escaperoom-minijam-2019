﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UIElements;

public class GameDirector : MonoBehaviour {
	public Queue<Command> commandsQueue;
	private float gameTime;
	public bool isCinematic;
	[HideInInspector] public UiManager uiManager;

	private Player player;

	private Dictionary<string, Actor> actors = new Dictionary<string, Actor>();
	private Hotspot[] hotspotsArray;
	private Dictionary<string, Hotspot> hotspots = new Dictionary<string, Hotspot>();
	private Dictionary<string, Item> items = new Dictionary<string, Item>();
	private Dictionary<string, StoryEvent> storyEvents = new Dictionary<string, StoryEvent>();
	private Dictionary<string, SAudio> audioFx = new Dictionary<string, SAudio>();

	private CursorManager cursorManager;

	private GameObject speechPrefab;
	private AudioSource audioSource;
	private CultureInfo cultureInfo;

	public event Action Skip;
	public event Action Interrupt;
	public event Action<string> BroadcastEvent;
	public event Action EnterCinematic;
	public event Action ExitCinematic;

	private void Awake() {
		cultureInfo = new CultureInfo("en-US");
		cursorManager = FindObjectOfType<CursorManager>();
		if (cursorManager == null) {
			Debug.LogError($"{name} missing CursorManager");
		}
		cursorManager.RoomInteraction += ManageClick;
		cursorManager.UiInteraction += ManageUiClick;

		Item[] loadedItems = Resources.LoadAll<Item>("Items/");
		foreach (var item in loadedItems) {
			items.Add(item.ID, item);
		}

		StoryEvent[] loadedStoryEvents = Resources.LoadAll<StoryEvent>("StoryEvents/");
		foreach (var storyEvent in loadedStoryEvents) {
			storyEvents.Add(storyEvent.name, storyEvent);
		}
		ResetStoryEvents();

		SAudio[] loadedAudioFx = Resources.LoadAll<SAudio>("SFX/");
		foreach (var audioEffect in loadedAudioFx) {
			audioFx.Add(audioEffect.ID, audioEffect);
		}

		player = FindObjectOfType<Player>();

		uiManager = FindObjectOfType<UiManager>();
		commandsQueue = new Queue<Command>();
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.loop = false;
		audioSource.playOnAwake = false;

		hotspotsArray = FindObjectsOfType<Hotspot>();
		foreach (var hotspot in hotspotsArray) {
			if (!hotspots.ContainsKey(hotspot.ID)) {
				hotspots.Add(hotspot.ID, hotspot);
			}
			else {
				Debug.LogWarning($"{name} has 2 or more hotspots with id {hotspot.ID}");
			}
		}
	}

	private void Start() {
		Actor[] actorsArray = FindObjectsOfType<Actor>();
		foreach (var actor in actorsArray) {
			actors.Add(actor.ID, actor);
		}
		gameTime = 0;
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
		if (Input.GetKeyDown(KeyCode.A)) {
			MoveActorCommand(player.ID, Vector3.zero, null);
			ExecuteQueue(true);
		}
		if (Input.GetKeyDown(KeyCode.S)) {
			SpeakActorCommand(player.ID, "Lorem Ipsum Dolet sit amet");
			ExecuteQueue(true);
		}
		if (Input.GetKeyDown(KeyCode.W)) {
			PlayAudioCommand("Beep");
			ExecuteQueue(true);
		}
		if (Input.GetKeyDown(KeyCode.L)) {
			TurnActorCommand(player.ID, Camera.main.transform.position);
			ExecuteQueue(true);
		}
		if (Input.GetKeyDown(KeyCode.Space)) {
			Skip?.Invoke();
		}
		if (Input.GetKeyDown(KeyCode.Q)) {
			if (commandsQueue.Count > 0) {
				ExecuteQueue();
			}
		}
		gameTime += Time.deltaTime;
	}

	private void ManageClick(Vector3 clickPosition, string targetId, InteractionType interactionType, Item tool) {
		if (isCinematic) {
			return;
		}

		Debug.Log($"{interactionType} on {targetId} with {((tool != null) ? tool.ID : "")}");

		if (!string.IsNullOrEmpty(targetId)) {
			if (GetItem(targetId) != null && interactionType == InteractionType.hand) {
				cursorManager.EquipTool(GetItem(targetId));
				return;
			}
			isCinematic = true;
			string commandsString = InteractionParser.Find(this, targetId, interactionType, tool).Trim();
			Debug.Log(commandsString);
			InterpretCommands(commandsString);
		}
		if (isCinematic) {
			StopAllCoroutines();
			EnterCinematic?.Invoke();
			StartCoroutine(ExecutingQueue());
		}
		else {
			MoveToClick(player.ID, isCinematic ? false : true);
		}
	}

	private void ManageUiClick(Vector3 clickPosition, string targetId, InteractionType interactionType, Item tool) {
		//skip click if is in Cinematic
		if (isCinematic) {
			return;
		}

		Debug.Log($"{interactionType} on {targetId} with {((tool != null) ? tool.ID : "")}");

		if (!string.IsNullOrEmpty(targetId)) {
			if (GetItem(targetId) != null && interactionType == InteractionType.hand) {
				cursorManager.EquipTool(GetItem(targetId));
				return;
			}
			string commandsString = InteractionParser.Find(this, targetId, interactionType, tool);
			InterpretCommands(commandsString);
			if (commandsQueue.Count > 0) {
				ExecuteQueue();
			}
		}
	}

	public void ExecuteQueue(bool cinematic = true) {
		isCinematic = cinematic;
		StopAllCoroutines();
		EnterCinematic?.Invoke();
		StartCoroutine(ExecutingQueue());
	}

	private IEnumerator ExecutingQueue() {
		while (commandsQueue.Count > 0) {
			Command c = commandsQueue.Dequeue();
			Debug.Log($"{name} executes {c} parameters {c.parameters} ({commandsQueue.Count} left)");
			c.Execute();
			while (c.running) {
				yield return null;
			}
		}
		isCinematic = false;
		ExitCinematic?.Invoke();
	}

	//TODO implementare i comandi non blocking
	public void InterpretCommands(string commandsString) {
		string[] commandStrings = commandsString.Split('\n', '\r');
		//Debug.Log(commandsString);
		foreach (var commandFull in commandStrings) {
			if (commandFull.Trim().Length == 0) {
				continue;
			}
			string tempCommandFull = commandFull.Trim();
			string command = tempCommandFull.Split(' ')[0];
			Regex regex;
			Match match;
			bool blocking = command[0] == '|';
			if (blocking) {
				command = command.Substring(1);
			}
			switch (command) {
				case "talk":
					regex = new Regex(CommandTalk.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						SpeakActorCommand(match.Groups[1].Value, match.Groups[2].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: {tempCommandFull}");
					}
					break;
				case "move":
					regex = new Regex(CommandMoveActor.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						string actorId = match.Groups[1].Value;
						string target = match.Groups[2].Value;
						Vector3 targetPosition;
						Quaternion? targetRotation = null;
						if (target[0] == '(') {
							targetPosition = new Vector3(float.Parse(match.Groups[5].Value, cultureInfo), float.Parse(match.Groups[6].Value, cultureInfo), float.Parse(match.Groups[7].Value, cultureInfo));
						}
						else {
							Hotspot targetHotspot = GetHotspot(target);
							targetPosition = targetHotspot.marker.transform.position;
							targetRotation = targetHotspot.marker.transform.localRotation;
						}
						MoveActorCommand(actorId, targetPosition, targetRotation);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "wait":
					regex = new Regex(CommandWait.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						WaitCommand(float.Parse(match.Groups[1].Value, cultureInfo));
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "get":
					regex = new Regex(CommandGetItem.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						AddItemCommand(match.Groups[1].Value, match.Groups[2].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "remove":
					regex = new Regex(CommandRemoveItem.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						RemoveItemCommand(match.Groups[1].Value, match.Groups[2].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "event":
					regex = new Regex(CommandInvokeGameEvent.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						InvokeGameEventCommand(match.Groups[1].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "set":
					regex = new Regex(CommandSetStoryEvent.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						SetStoryEventCommand(match.Groups[1].Value, match.Groups[2].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "look":
					regex = new Regex(CommandLookAt.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						float duration = (match.Groups[9].Value.Length > 0) ? float.Parse(match.Groups[9].Value, cultureInfo) : 0.5f;

						if (match.Groups[2].Value[0] == '(') {
							Vector3 targetPosition = new Vector3(float.Parse(match.Groups[5].Value, cultureInfo), float.Parse(match.Groups[6].Value, cultureInfo), float.Parse(match.Groups[7].Value, cultureInfo));
							TurnActorCommand(match.Groups[1].Value, targetPosition, duration);
						}
						else {
							TurnActorCommand(match.Groups[1].Value, GetHotspot(match.Groups[2].Value).transform.position, duration);
						}
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				case "play":
					regex = new Regex(CommandPlaySound.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						PlayAudioCommand(match.Groups[1].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				//Puo' essere "game" o "other" 
				case "cursor":
					regex = new Regex(CommandCursorActivator.Regex);
					match = regex.Match(tempCommandFull);
					if (match.Success) {
						CursorActivatorCommand(match.Groups[1].Value);
					}
					else {
						SpeakActorCommand("player", $"COMMAND [{tempCommandFull}] WITH ERRORS");
						Debug.LogError($"Syntax Error: [{tempCommandFull}]");
					}
					break;
				default:
					SpeakActorCommand("player", $"COMMAND [{command}] NOT FOUND");
					Debug.LogError($"COMMAND [{command}] NOT FOUND ");
					break;
			}
		}
	}

	private void MoveToClick(string actorId, bool important) {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 1000f)) {
			Vector3 destination = hit.point;
			GameObject clicked = hit.collider.gameObject;
			//check if clicked on hotspot
			Hotspot clickedHotspot = clicked.GetComponent<Hotspot>();
			if (clickedHotspot != null) {
				destination = clickedHotspot.marker.transform.position;
				actors[actorId].matchRotation = clickedHotspot.marker.transform.localRotation;
			}
			else {
				actors[actorId].matchRotation = null;
			}

			if (important) {
				commandsQueue.Clear();
				Interrupt?.Invoke();
			}

			MoveActorCommand(actorId, destination, actors[actorId].matchRotation);

			if (important) {
				StopAllCoroutines();
				StartCoroutine(ExecutingQueue());
			}
		}
	}

	public void MoveActorCommand(string actorId, Vector3 targetPosition, Quaternion? finalRotation, bool blocking = true) {
		Command move = new CommandMoveActor(this);
		string[] parameters;
		if (finalRotation != null) {
			parameters = new[]
			{
				actorId, targetPosition.x.ToString(), targetPosition.y.ToString(), targetPosition.z.ToString(), finalRotation.Value.x.ToString(), finalRotation.Value.y.ToString(), finalRotation.Value.z.ToString(),
				finalRotation.Value.w.ToString()
			};
		}
		else {
			parameters = new[] { actorId, targetPosition.x.ToString(), targetPosition.y.ToString(), targetPosition.z.ToString() };
		}
		move.parameters = parameters;
		commandsQueue.Enqueue(move);
	}

	private void AddItemCommand(string actorId, string itemId) {
		Command addItem = new CommandGetItem(this);
		addItem.parameters = new[] { actorId, itemId };
		commandsQueue.Enqueue(addItem);
	}

	private void RemoveItemCommand(string actorId, string itemId) {
		Command removeItem = new CommandRemoveItem(this);
		removeItem.parameters = new[] { actorId, itemId };
		commandsQueue.Enqueue(removeItem);
	}

	public void TurnActorCommand(string actorId, Vector3 targetPosition, float duration = .5f, bool blocking = true) {
		Command turn = new CommandLookAt(this);
		turn.parameters = new[] { actorId, targetPosition.x.ToString(), targetPosition.y.ToString(), targetPosition.z.ToString(), duration.ToString() };
		commandsQueue.Enqueue(turn);
	}

	public void SpeakActorCommand(string actorId, string textToShow, bool blocking = true) {
		Command talk = new CommandTalk(this);
		talk.parameters = new[] { actorId, textToShow };
		commandsQueue.Enqueue(talk);
	}

	public void WaitCommand(float duration, bool blocking = true) {
		Command wait = new CommandWait(this);
		wait.parameters = new[] { duration.ToString() };
		commandsQueue.Enqueue(wait);
	}

	public void CursorActivatorCommand(string mode) {
		Command cursorActivator = new CommandCursorActivator(this);
		cursorActivator.parameters = new[] { mode };
		commandsQueue.Enqueue(cursorActivator);
	}

	public void InvokeGameEventCommand(string eventName) {
		Command invoke = new CommandInvokeGameEvent(this);
		invoke.parameters = new[] { eventName };
		commandsQueue.Enqueue(invoke);
	}

	public void PlayAudioCommand(string audioFxId, bool blocking = true) {
		Command play = new CommandPlaySound(this);
		play.parameters = new[] { audioFxId };
		commandsQueue.Enqueue(play);
	}

	public void InvokeEventInstantly(string eventName) {
		Debug.Log($"{name} is invoking {eventName}");
		BroadcastEvent?.Invoke(eventName);
	}

	public void SetStoryEventCommand(string storyEventName, string newValue) {
		Command setStoryEvent = new CommandSetStoryEvent(this);
		setStoryEvent.parameters = new[] { storyEventName, newValue };
		commandsQueue.Enqueue(setStoryEvent);
	}

	public void EnableCursorManager(bool active) {
		cursorManager.SetActive(active);
	}

	public AudioSource GetAudioSource() {
		return audioSource;
	}

	public void PlayAudio(string audioFxId) {
		if (audioFx.ContainsKey(audioFxId)) {
			SAudio audioEffect = audioFx[audioFxId];
			audioEffect.Play(audioSource);
		}
	}

	private void ResetStoryEvents() {
		foreach (var storyEvent in storyEvents) {
			storyEvent.Value.Reset();
		}
	}

	public StoryEvent GetStoryEvent(string storyEventName) {
		return storyEvents.ContainsKey(storyEventName) ? storyEvents[storyEventName] : null;
	}

	public Item GetItem(string itemId) {
		return items.ContainsKey(itemId) ? items[itemId] : null;
	}

	public Hotspot GetHotspot(string hotspotId) {
		return hotspots.ContainsKey(hotspotId) ? hotspots[hotspotId] : null;
	}

	public Hotspot[] GetHotspots() {
		return hotspotsArray;
	}

	public Actor GetActor(string actorId) {
		if (!actors.ContainsKey(actorId)) {
			Debug.LogError($"{actorId} not found in Actors Dictionary");
			return null;
		}
		return actors[actorId];
	}

	public Player GetPlayer() {
		return player;
	}
}