﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEngine;

public static class InteractionParser {
	private const string XMLInputFilename = "Interactions";
	private const string conditionRegex = @"(!actorHasItem|actorHasItem|compareStoryEvent)\s(\w+)\s([\w\W]+)";
	private static XmlDocument xml;

	public static string Find(GameDirector director, string targetId, InteractionType type, Item tool) {
		bool hasTool = tool != null;
		string toolId = hasTool ? tool.ID : "";
		if (xml == null) {
			Debug.Log($"InteractionParser reads xml");
			TextAsset xmlContents = Resources.Load<TextAsset>(XMLInputFilename);
			xml = new XmlDocument();
			xml.LoadXml(xmlContents.text);
		}

		string path = $"//interactive[@name='{targetId}']/interaction[@type='{type}']";
		if (type == InteractionType.tool) {
			path = $"//interactive[@name='{targetId}']/interaction[@tool='{tool.ID}']";
		}

		XmlNodeList nodes = xml.SelectNodes(path);
		Debug.Log($"path: {path} nodes: {nodes.Count}");

		var possibleOutcomes = new List<string>();

		if (nodes.Count > 0) {
			foreach (XmlElement node in nodes) {
				bool conditionsVerified = true;
				string consequences = "";

				XmlNodeList targetNodeConditions = node.SelectNodes("./conditions");
				if (targetNodeConditions.Count >= 1) {
					conditionsVerified = CheckConditions(director, targetNodeConditions);
				}

				XmlNodeList targetNodeConsequences = node.SelectNodes("./consequences");
				foreach (XmlElement consequence in targetNodeConsequences) {
					consequences += consequence.InnerText + " ";
				}
				if (conditionsVerified) {
					possibleOutcomes.Add(consequences.Trim());
					//return consequences.Trim();
				}
			}

			if (possibleOutcomes.Count > 0) {
				return possibleOutcomes[Random.Range(0, possibleOutcomes.Count)];
			}
			else {
				return $"look player {targetId} \ntalk player Can't do that now";
			}
		}
		else {
			return $"look player {targetId} \ntalk player This is quite inappropriate";
		}
	}

	private static bool CheckConditions(GameDirector director, XmlNodeList targetNodeConditions) {
		XmlNode conditionNode = targetNodeConditions[0];
		bool conditionsVerified = true;
		string[] conditions = conditionNode.InnerText.Trim().Split('\r', '\n');
		var regex = new Regex(conditionRegex);
		foreach (var condition in conditions) {
			Match match = regex.Match(condition.Trim());
			if (match.Success) {
				//ora analizzo la condizione in se'
				switch (match.Groups[1].ToString()) {
					case "actorHasItem":
						conditionsVerified &= director.GetActor(match.Groups[2].ToString()).HasItem(director.GetItem(match.Groups[3].ToString()));
						break;
					case "!actorHasItem":
						conditionsVerified &= !director.GetActor(match.Groups[2].ToString()).HasItem(director.GetItem(match.Groups[3].ToString()));
						break;
					case "compareStoryEvent":
						string storyEventName = match.Groups[2].ToString();
						if (director.GetStoryEvent(storyEventName) != null) {
							conditionsVerified &= director.GetStoryEvent(storyEventName).value == match.Groups[3].ToString();
						}
						else {
							Debug.LogError($"Can't find Story Event [{storyEventName}]");
							conditionsVerified = false;
						}
						break;
				}
			}
			if (!conditionsVerified) {
				break;
			}
		}
		return conditionsVerified;
	}
}