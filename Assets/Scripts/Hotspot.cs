﻿using System;
using UnityEditor;
using UnityEngine;

public class Hotspot : MonoBehaviour, Interactive {
	[SerializeField] private string id;
	public string ID {
		get => id;
	}
	[TextArea] public string description;
	public GameObject marker;
	private CursorManager cursorManager;

	public event Action<string, string> MouseOver;
	public event Action<string> MouseExit;

	private void Awake() {
		cursorManager = FindObjectOfType<CursorManager>();
		if (id.Length == 0) {
			id = name;
		}
		if (marker == null) {
			marker = new GameObject("Marker");
			marker.transform.parent = gameObject.transform;
			marker.transform.localPosition = Vector3.zero;
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos() {
		if (marker != null) {
			Vector3 position = marker.transform.position;
			Gizmos.DrawWireSphere(position, .2f);
			Gizmos.DrawLine(position, position + marker.transform.rotation * Vector3.forward);
			Handles.Label(position, $"{id} hotspot marker");
			Gizmos.DrawIcon(transform.position,"hotspot.png");
		}
	}
#endif

	public void OnLeftClick() {
		throw new NotImplementedException();
	}

	public void OnRightClick(Item tool = null) {
		throw new NotImplementedException();
	}

	private void OnMouseOver() {
		MouseOver?.Invoke(ID, description);
		// cursorManager.SetTarget(ID, description);
		// cursorManager.ShowText();
		// cursorManager.ShowActions();
	}

	private void OnMouseExit() {
		MouseExit?.Invoke(ID);
		// cursorManager.UnsetTarget(ID);
		// cursorManager.HideText();
		// cursorManager.HideActions();
	}
}