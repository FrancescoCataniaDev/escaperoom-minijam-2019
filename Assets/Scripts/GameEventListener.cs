﻿using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour {
	public string listenToEvent;
	public UnityEvent response;
	private GameDirector director;

	private void Awake() {
		director = FindObjectOfType<GameDirector>();
	}

	private void OnEnable() {
		director.BroadcastEvent += OnEventRaised;
	}

	private void OnDisable() {
		director.BroadcastEvent -= OnEventRaised;
	}

	private void OnEventRaised(string eventName) {
		if (eventName == listenToEvent) {
			Debug.Log($"{name} hears {eventName}");
			response.Invoke();
		}
	}
}