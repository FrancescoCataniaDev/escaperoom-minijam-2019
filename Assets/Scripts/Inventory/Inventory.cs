﻿using System;
using System.Collections.Generic;

public class Inventory {
	public Dictionary<string, Item> items;
	public event Action<Dictionary<string, Item>> Updated;

	public Inventory(List<Item> startingItems = null) {
		items = new Dictionary<string, Item>();
		if (startingItems != null) {
			foreach (var item in startingItems) {
				AddItem(item);
			}
		}
	}

	public void AddItem(Item newItem) {
		if (!HasItem(newItem)) {
			items.Add(newItem.ID, newItem);
			Updated?.Invoke(items);
		}
	}

	public bool HasItem(Item item) {
		return HasItem(item.ID);
	}

	public bool HasItem(string id) {
		return items.ContainsKey(id);
	}

	public void RemoveItem(Item newItem) {
		items.Remove(newItem.ID);
		Updated?.Invoke(items);
	}

	public void Empty() {
		items.Clear();
		Updated?.Invoke(items);
	}

	public int GetSize() {
		return items.Count;
	}
}