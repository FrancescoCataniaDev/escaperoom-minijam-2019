﻿using UnityEngine;

[CreateAssetMenu(menuName = "RoomEscape/Item")]
public class Item : ScriptableObject {
	[SerializeField] private string id = "id";
	public string ID {
		get => id;
	}
	public string description;
	public Sprite inventorySprite;
}