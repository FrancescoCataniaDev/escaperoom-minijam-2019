﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour, Interactive, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {
	public Item item;
	//public event Action<string, string> ItemHover;
	public Image uiImage;
	private CursorManager cursorManager;

	private void Awake() {
		cursorManager = FindObjectOfType<CursorManager>();
		if (uiImage == null) {
			uiImage = GetComponentInChildren<Image>();
		}
	}

	public void SetImage() {
		uiImage.sprite = item.inventorySprite;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		cursorManager.SetTarget(item.ID, item.description);
		cursorManager.ShowText();
		cursorManager.ShowActions();
	}

	public void OnPointerExit(PointerEventData eventData) {
		cursorManager.UnsetTarget(item.ID);
		cursorManager.HideText();
		cursorManager.HideActions();
	}

	public void OnPointerClick(PointerEventData eventData) { }

	public void OnPointerDown(PointerEventData eventData) {
		//throw new NotImplementedException();
	}

	public void OnPointerUp(PointerEventData eventData) {
		//throw new NotImplementedException();
	}

	public void OnLeftClick() {
		// switch (cursorManager.leftInteractionType) {
		// 	case InteractionType.hand:
		// 		cursorManager.SetTool(item);
		// 		break;
		// 	case InteractionType.eye:
		// 		break;
		// 	case InteractionType.tool:
		// 		break;
		// 	case InteractionType.cancel:
		// 		break;
		// 	default:
		// 		throw new ArgumentOutOfRangeException();
		// }
	}

	public void OnRightClick(Item tool = null) {
		// switch (cursorManager.rightInteractionType) {
		// 	case InteractionType.hand:
		// 		break;
		// 	case InteractionType.eye:
		// 		break;
		// 	case InteractionType.tool:
		// 		break;
		// 	case InteractionType.cancel:
		// 		break;
		// 	default:
		// 		throw new ArgumentOutOfRangeException();
		// }
	}
}