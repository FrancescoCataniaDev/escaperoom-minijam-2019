﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class CameraPan : MonoBehaviour {
	public GameObject followedObject;
	public bool x;
	public bool y;
	public bool z;
	[ShowIf("x")] public Vector2 xBoundaries;
	[ShowIf("y")] public Vector2 yBoundaries;
	[ShowIf("z")] public Vector2 zBoundaries;
	private Vector3 targetPosition;
	private Vector3 startingOffset;
	[ShowInInspector, ReadOnly] private Vector3 offset;
	private Vector3 startingPosition;
	private Vector3 startingTargetPosition;

	private void Awake() {
		Init();
	}

	public void Init() {
		if (followedObject != null) {
			targetPosition = followedObject.transform.position;
			startingTargetPosition = targetPosition;
			startingPosition = transform.position;
			startingOffset = startingPosition - targetPosition;
		}
		else {
			Debug.LogWarning($"{name} Camera Pan has no target");
			enabled = false;
		}
	}

	private void Update() {
		offset = transform.position - startingPosition;
		Vector3 targetTempPosition = followedObject.transform.position;
		targetPosition = new Vector3(x ? (Mathf.Clamp(targetTempPosition.x,xBoundaries.x,xBoundaries.y)) : startingTargetPosition.x, 
			y ? (targetTempPosition.y) : startingTargetPosition.y, 
			z ? (targetTempPosition.z) : startingTargetPosition.z) + startingOffset;
		transform.position = Vector3.Lerp(transform.position, targetPosition, 0.5f);
	}
}