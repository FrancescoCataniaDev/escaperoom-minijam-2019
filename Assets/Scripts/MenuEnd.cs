﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuEnd : MonoBehaviour {
	private CanvasGroup canvasGroup;

	private void Awake() {
		canvasGroup = GetComponent<CanvasGroup>();
		canvasGroup.alpha = 0;
		canvasGroup.interactable = false;
		canvasGroup.blocksRaycasts = false;
	}

	public void ShowMenu() {
		canvasGroup.interactable = true;
		canvasGroup.blocksRaycasts = true;
		canvasGroup.DOFade(1, .5f);
	}

	public void StartGame() {
		SceneManager.LoadScene("GraphicSceneTest");
	}

	public void Quit() {
		Application.Quit();
	}
}