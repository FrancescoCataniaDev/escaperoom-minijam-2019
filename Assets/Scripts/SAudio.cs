﻿using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "NewSAudio", menuName = "RoomEscape/Audio Fx")]
public class SAudio : ASAudioEvent {
	[SerializeField] private string id = "id";
	public string ID {
		get => id;
	}
	public AudioClip[] audioClips;
	public float pitchVariation;
	public float volumeVariation;
	public float pitchBase;
	public float volumeBase;

	public override void Play(AudioSource audioSource) {
		Play(audioSource, Random.Range(0, audioClips.Length));
	}

	private void OnEnable() {
		volumeBase = Mathf.Clamp(volumeBase, 0, 1f);
	}

	public override void Play(AudioSource audioSource, int clipNumber = 0) {
		if (audioClips.Length == 0) return;

		audioSource.clip = audioClips[clipNumber % audioClips.Length];
		audioSource.pitch = pitchBase + Random.Range(-pitchVariation, +pitchVariation);
		audioSource.volume = volumeBase + Random.Range(0, -volumeVariation * 2);

		if (audioSource.isActiveAndEnabled) {
			audioSource.Play();
		}
	}
}