﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RoomEscape/StoryEvent")]
public class StoryEvent : ScriptableObject {
	public string value;
	public string defaultValue;

	public void Reset() {
		value = defaultValue;
	}
}